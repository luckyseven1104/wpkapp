'use strict';

/**
 * @ngdoc function
 * @name wpkFrontEndApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the wpkFrontEndApp
 */
angular.module('wpkFrontEndApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
