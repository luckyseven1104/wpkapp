'use strict';

/**
 * @ngdoc function
 * @name wpkFrontEndApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the wpkFrontEndApp
 */
angular.module('wpkFrontEndApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
